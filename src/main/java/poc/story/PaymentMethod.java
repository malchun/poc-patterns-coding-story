package poc.story;

public enum PaymentMethod {
    CREDIT_CARD,
    PAYPAL,
    GIFT_CARD
}
