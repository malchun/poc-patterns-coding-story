package poc.story;

import lombok.AllArgsConstructor;

@AllArgsConstructor
public class StripePaymentCommand implements PaymentCommand {
    private final PaymentData payment;
    private StripeClient stripeClient;

    private boolean charged = false;

    StripePaymentCommand(PaymentData payment) {
        this.payment = payment;
    }

    public void charge() {
        this.charged = true;
        this.stripeClient.capture(this.payment);
        // other stripe related actions
    }

    public void revert() {
        if (this.charged) {
            this.stripeClient.refund(this.payment);
        }
    }
}