package poc.story;

public class StripeClient {

    public void hold(PaymentData payment) {
        // some stripe related actions
    }

    public void capture(PaymentData payment) {
        // some stripe related actions
    }

    public void refund(PaymentData payment) {
        // some stripe related actions
    }
}
