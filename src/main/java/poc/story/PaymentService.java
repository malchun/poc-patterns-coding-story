package poc.story;

import java.util.ArrayList;
import java.util.List;

public class PaymentService {

    public void processPayments(List<PaymentCommand> payments) {
        try {
            payments.forEach(PaymentCommand::charge);
        } catch (Exception e) {
            payments.forEach(PaymentCommand::revert);
        }
    }
}