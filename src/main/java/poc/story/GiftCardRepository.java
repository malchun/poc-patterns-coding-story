package poc.story;

public class GiftCardRepository {

    public void validate(PaymentData payment) {
        // some gift card related actions
    }

    public void charge(PaymentData payment) {
        // some gift card related actions
    }

    public void refund(PaymentData payment) {
        // some gift card related actions
    }
}
