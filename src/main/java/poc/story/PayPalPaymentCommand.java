package poc.story;

import lombok.AllArgsConstructor;

@AllArgsConstructor
class PayPalPaymentCommand implements PaymentCommand {
    private PayPalClient payPalClient;
    private final PaymentData payment;

    private boolean charged = false;

    PayPalPaymentCommand(PaymentData payment) {
        this.payment = payment;
    }

    public void charge() {
        this.payPalClient.hold(this.payment);
        this.payPalClient.capture(this.payment);
        // other paypal related actions
        this.charged = true;
    }

    public void revert() {
        if (this.charged) {
            this.payPalClient.refund(this.payment);
        }
    }
}
