package poc.story;

public class PayPalClient {

    public void hold(PaymentData payment) {
        // some paypal related actions
    }

    public void capture(PaymentData payment) {
        // some paypal related actions
    }

    public void refund(PaymentData payment) {
        // some paypal related actions
    }
}
