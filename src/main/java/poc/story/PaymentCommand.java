package poc.story;

public interface PaymentCommand {
    void charge();

    void revert();
}
