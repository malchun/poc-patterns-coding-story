package poc.story;

import lombok.AllArgsConstructor;

import java.util.List;

@AllArgsConstructor
class PaymentController {

    private PaymentService paymentService;

    void processPayments(List<PaymentData> payments) {
        var paymentCommands = payments.stream()
                .map(this::createPaymentCommand)
                .toList();
        this.paymentService.processPayments(paymentCommands);
    }

    private PaymentCommand createPaymentCommand(PaymentData paymentData) {
        return switch (paymentData.getMethod()) {
            case PaymentMethod.GIFT_CARD -> new GiftCardPaymentCommand(paymentData);
            case PaymentMethod.PAYPAL -> new PayPalPaymentCommand(paymentData);
            case PaymentMethod.CREDIT_CARD -> new StripePaymentCommand(paymentData);
        };
    }
}