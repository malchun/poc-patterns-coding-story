package poc.story;

public class GiftCardPaymentCommand implements PaymentCommand {
    private GiftCardRepository giftCardRepository;
    private final PaymentData payment;
    private boolean charged = false;

    GiftCardPaymentCommand(PaymentData payment) {
        this.payment = payment;
    }

    public void charge() {
        this.giftCardRepository.validate(this.payment);
        this.giftCardRepository.charge(this.payment);
        // other gift card related actions
        this.charged = true;
    }

    public void revert() {
        if (this.charged) {
            this.giftCardRepository.refund(this.payment);
        }
    }
}