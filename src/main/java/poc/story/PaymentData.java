package poc.story;

import lombok.Data;

@Data
public class PaymentData {
    // some information related to payment
    PaymentMethod method;
}
