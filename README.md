# E-Commerse implementation with command story
**Estimated reading time**: 1 hour

## Story Outline
Let's say you are working on an e-commerce solution and one of the requirements is integration with multiple payment methods such as PayPal, Stripe (credit card), and gift cards. The customer can split the payment between any method and a gift card. Each payment method has its own unique sequence of actions that must be done to perform or return a payment. But there is also one important requirement: to be able to undo all changes made to any payment method if something goes wrong, so you must keep track of what has been done.
This story will walk you through the steps necessary to implement this functionality using the Command pattern.

## Story Organization
**Story Branch**: main
> `git checkout main`